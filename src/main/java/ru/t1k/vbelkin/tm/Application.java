package ru.t1k.vbelkin.tm;

import ru.t1k.vbelkin.tm.constant.TerminalConst;

import static ru.t1k.vbelkin.tm.constant.TerminalConst.VERSION;
import static ru.t1k.vbelkin.tm.constant.TerminalConst.HELP;
import static ru.t1k.vbelkin.tm.constant.TerminalConst.ABOUT;

public class Application {

    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            showErrorArgument();
            return;
        }
        final String arg = args[0];
        switch (arg) {
            case VERSION:
                showVersion();
                break;
            case ABOUT:
                showAbout();
                break;
            case HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    public static void showErrorArgument() {
        System.err.println("Error! This argument not supported...");
    }

    public static void showVersion() {
        System.out.println("[Version]");
        System.out.println("1.5.0");
    }

    public static void showAbout() {
        System.out.println("[About]");
        System.out.println("Name: Vadim Belkin");
        System.out.println("E-mail: vbelkin@tsconsulting.ru");
        System.out.println("Web-Site: https://gitlab.com/VBELKIN");
    }

    public static void showHelp() {
        System.out.println("[Help]");
        System.out.printf("%s - Show application version. \n", TerminalConst.VERSION);
        System.out.printf("%s - Show developer info. \n", TerminalConst.ABOUT);
        System.out.printf("%s - Show application commands. \n", TerminalConst.HELP);
    }

}
